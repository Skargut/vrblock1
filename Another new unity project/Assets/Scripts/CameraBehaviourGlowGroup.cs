﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraBehaviourGlowGroup : MonoBehaviour
{
    /* What is this class supposed to do:
     * Record your voice when you're looking at your "companion"
     * Highlight objects when looked at AFTER you haven't said anything.
     * Show small tips (floating text) AFTER you haven't said anything even longer.
     * (Move the camera around using keyboard input {only for pc testing purposes})
     */
     
    public float speed;
    public float range;
    public string ObjectTag;
    public string CompanionTag;
    public int clueDelay;

    public GameObject clueText;
    //public Material newMaterial;

    private string previousHitName = "";
    private string previousHitTag = "";
    
    public bool isLookingAtCompanion = false;
    
    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(Input.GetAxis("Vertical") * -speed * Time.deltaTime, Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0);

        RaycastHit hit;
        Ray landingRay = new Ray(transform.position, transform.TransformDirection(Vector3.forward));
        
        if (Physics.Raycast(landingRay, out hit, range))
        {
            string hitName = hit.transform.gameObject.name;
            string hitTag = hit.transform.gameObject.tag;
            
            if (hitTag == CompanionTag)
            {
                isLookingAtCompanion = true;
            } else if (hitTag != CompanionTag || hitTag == null) 
            {
                if ((previousHitName != hitName || previousHitTag != hitTag) && (previousHitName != "" || previousHitTag != "")) { RemoveColour(); }

                if (hitTag == ObjectTag) SetColour(hit.transform.gameObject);
                isLookingAtCompanion = false;
            }
        }
        else
        {
            isLookingAtCompanion = false;
            if (previousHitTag != "") RemoveColour();
        }
    }
    
    
    // Resets the colour of the previously looked at object.
    void RemoveColour()
    {
        StopAllCoroutines();
        RemoveText();
        GameObject[] checkObjects = GameObject.FindGameObjectsWithTag(previousHitTag);
        for (int i = 0; i < checkObjects.Length; i++)
        {
            if (checkObjects[i].name == previousHitName)
            {
                GameObject checkObject = GameObject.FindGameObjectWithTag("Glow");
                Destroy(checkObject);
                previousHitName = "";
                previousHitTag = "";
            }
        }
    }

    // Changes the colour of an object you are looking at.
    void SetColour(GameObject hit)
    {
        if (hit.transform.gameObject.name != previousHitName && hit.transform.gameObject.tag != previousHitTag)
        {
            StartCoroutine(SetText(hit.gameObject));
            previousHitName = hit.name;
            previousHitTag = hit.tag;

            Instantiate(hit, hit.transform.position, hit.transform.rotation);
            string objectname = hit.name + "(Clone)";
            GameObject[] checkObjects = GameObject.FindGameObjectsWithTag(ObjectTag);
            for (int i = 0; i < checkObjects.Length; i++)
            {
                if (checkObjects[i].name == objectname)
                {
                    Transform[] checkTransform =  checkObjects[i].GetComponentsInChildren<Transform>();
                    for (int o = 0; o < checkTransform.Length; o++)
                    {
                        checkTransform[o].gameObject.transform.localScale = (checkTransform[o].gameObject.tag == "Obstacle" && checkTransform.Length != 1) ? new Vector3(1f, 1f, 1f) : hit.transform.localScale + new Vector3(0.1f, 0.1f, 0.1f);
                        checkTransform[o].gameObject.name = checkTransform[o].gameObject.tag = "Glow";
                        checkTransform[o].gameObject.transform.rotation = checkTransform[o].gameObject.transform.rotation;
                        //checkTransform[o].gameObject.GetComponent<MeshRenderer>().material = newMaterial;
                        checkTransform[o].gameObject.GetComponent<Collider>().enabled = false;
                    }
                }
            }
        }
    }

    // Displays text after looking at it for X seconds.
    IEnumerator SetText(GameObject hit)
    {
        yield return new WaitForSeconds(clueDelay);
        clueText.GetComponent<TextMesh>().text = hit.GetComponent<Text>().text;
        Instantiate(clueText, hit.transform.position, Quaternion.Euler(0, gameObject.transform.eulerAngles.y, 0));
    }

    // Removes text after you've stopped looking at an object.
    void RemoveText()
    {
        Destroy(GameObject.Find("ClueText(Clone)"));
    }
    
}