﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    /* Stuff that still has to be added: 
     * incorrect responses for phase.
     * silent response audioclips for phase
     * multiple words can be recognized for the same result.
     *  - specific results, but also an option to respond when the player says anything (Recognized word should be "-").
     * audiofiles that keep playing while you're not speaking.
     * When holding button for x seconds -> The game restarts
     * You can interrupt the silence by speaking, you don't have to wait the entire line.
     */

    public AudioSource audioSource;
    public TextAsset dataFile;
    public string audioLocation;

    public AudioClip startAudioClip; 

    public int maxSilenceDuration;

    public List<string> activePhases                       = new List<string>();
    public List<int> phaseLength                           = new List<int>();
    public List<bool> phaseCompletion                      = new List<bool>();
    public List<string> phaseOptions                       = new List<string>();
    public List<List<string>> optionResponseAudioClips     = new List<List<string>>();
    public List<string> phaseWrongOptionResponsAudioClips  = new List<string>();
    public List<string> wrongOptionResponseAudioClips      = new List<string>();
    public List<string> phaseSilentResponseAudioClips      = new List<string>();
    public List<string> silentResponseAudioClips           = new List<string>();

    public bool isSpeaking = false;
    public int notResponding = 0;
    
	// Use this for initialization
	void Start () {
        AddPhase(0);
        wrongOptionResponseAudioClips = LoadText("WRONG ANSWER GENERAL START", "WRONG ANSWER GENERAL END");
        silentResponseAudioClips = LoadText("SILENT ANSWER GENERAL START", "SILENT ANSWER GENERAL END");
        wrongOptionResponseAudioClips.RemoveAt(0);
        silentResponseAudioClips.RemoveAt(0);
        StartCoroutine(FirstSpeech());
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(CheckSpeech("This is some cool input."));
        }
    }
    /// <summary>
    /// The speech that is played at the start of the game.
    /// </summary>
    /// <returns></returns>
    IEnumerator FirstSpeech()
    {
        audioSource.PlayOneShot(startAudioClip);
        isSpeaking = true;
        yield return new WaitForSeconds(startAudioClip.length);
        isSpeaking = false;
        StartCoroutine(NoSpeech());
    }
    /// <summary>
    /// Checks the received input on the keywords that are used inside the current phase.
    /// This function is spread over X frames to improve framerate.
    /// Only works when the game is not speaking at that moment.
    /// </summary>
    /// <param name="receivedSpeech"></param>
    /// <returns></returns>
    public IEnumerator CheckSpeech(string receivedSpeech)
    {
        if (!isSpeaking)
        {
            isSpeaking = true;
            bool foundCorrectTerm = false;
            for (int i = 0; i < phaseOptions.Count && foundCorrectTerm == false; i++)
            {
                int wordLenght = phaseOptions[i].Length;
                for (int o = 0; o + wordLenght < receivedSpeech.Length + 1; o++)
                {
                    string currentWord = receivedSpeech.Substring(o, wordLenght).ToLower();
                    if ((currentWord == phaseOptions[i].ToLower() || phaseOptions[i] == "-") && !phaseCompletion[i])
                    {
                        phaseCompletion[i] = true;
                        foundCorrectTerm = true;
                        StartCoroutine(GetSoundCorrect(i + 1));
                    }
                    yield return new WaitForEndOfFrame();
                }
                if (receivedSpeech.Length < 2 && !foundCorrectTerm) StartCoroutine(GetSoundIncorrect(i));
            }
            if (receivedSpeech.Length > 1 && !foundCorrectTerm) StartCoroutine(GetSoundIncorrect());
        }
    }
    /// <summary>
    /// Checks for how long you haven't spoken.
    /// Plays AudioClip when silent for too long.
    /// </summary>
    /// <returns></returns>
    IEnumerator NoSpeech()
    {
        AudioClip audioClip = new AudioClip();
        while (true)
        {
            if (isSpeaking)
            {
                notResponding = 0;
            }
            else
            {
                notResponding++;
            }
            if (notResponding == maxSilenceDuration)
            {
                if (activePhases.Count == 1 && phaseSilentResponseAudioClips.Count != 0)
                {
                    Debug.Log("singlephase silent");
                    audioClip = Resources.Load(audioLocation + phaseSilentResponseAudioClips[UnityEngine.Random.Range(0, phaseSilentResponseAudioClips.Count)]) as AudioClip;
                } else
                {
                    Debug.Log("morephase silent");
                    audioClip = Resources.Load(audioLocation + silentResponseAudioClips[UnityEngine.Random.Range(0, silentResponseAudioClips.Count)]) as AudioClip;
                }
                audioSource.PlayOneShot(audioClip);
                isSpeaking = true;
                yield return new WaitForSeconds(audioClip.length);
                isSpeaking = false;
                notResponding = 0;
            }
            yield return new WaitForSeconds(1);
        }
    }
    /// <summary>
    /// Selects one of the X possible responses that you are able to get.
    /// </summary>
    /// <param name="optionNo"></param>
    /// <returns></returns>
    IEnumerator GetSoundCorrect(int optionNo)
    {
        Debug.Log("correct");
        List<string> audioFiles = optionResponseAudioClips[optionNo];
        AudioClip audioClip = Resources.Load(audioLocation + audioFiles[UnityEngine.Random.Range(0, audioFiles.Count)]) as AudioClip;
        if (audioClip != null) audioSource.PlayOneShot(audioClip);
        yield return new WaitForSeconds(audioClip.length);
        isSpeaking = false;
        CheckPhaseCompletion();
    }
    /// <summary>
    /// Plays one of the X possible sounds when no keywords are recognized.
    /// Is used when there is only 1 phase active.
    /// </summary>
    /// <returns></returns>
    IEnumerator GetSoundIncorrect(int phaseIndexNo)
    {
        int thisPhaseLenght = phaseLength[phaseIndexNo];
        int startIndex = 1;
        for (int i = 0; i < phaseIndexNo; i++)
        {
            startIndex += phaseLength[i];
        }
        AudioClip audioClip = Resources.Load(audioLocation + phaseWrongOptionResponsAudioClips[UnityEngine.Random.Range(startIndex, startIndex + thisPhaseLenght)]) as AudioClip;
        if (audioClip != null) audioSource.PlayOneShot(audioClip);
        yield return new WaitForSeconds(audioClip.length);
        isSpeaking = false;
    }
    /// <summary>
    /// Plays one of the X possible sounds when no keywords are recognized. 
    /// Is used when there are more than 1 phase active.
    /// </summary>
    /// <returns></returns>
    IEnumerator GetSoundIncorrect()
    {
        AudioClip audioClip = Resources.Load(audioLocation + wrongOptionResponseAudioClips[UnityEngine.Random.Range(0, wrongOptionResponseAudioClips.Count)]) as AudioClip;
        if (audioClip != null) audioSource.PlayOneShot(audioClip);
        yield return new WaitForEndOfFrame();
        isSpeaking = false;
    }
    /// <summary>
    /// Checks all phases on whether all their keywords have been triggered.
    /// And Removes the phase that has been completed.
    /// </summary>
    void CheckPhaseCompletion()
    {
        int startIndex = 0;
        for (int i = 0; i < phaseLength.Count; i ++)
        {
            bool correct = true;
            for (int o = 0; o < phaseLength[i]; o++)
            {
                if (!phaseCompletion[o + startIndex]) correct = false;
            }
            if (correct)
            {
                RemovePhase(i);
                Debug.Log(activePhases[i]);
                AddPhase(int.Parse(activePhases[i]));
            }
            startIndex += phaseLength[i];
        }
    }
    /// <summary>
    /// Removes all variables from included phase.
    /// the included phase is the phase's POSITION INDEX inside the list.
    /// </summary>
    /// <param name="phase"></param>
    void RemovePhase(int phaseIndex)
    {
        Debug.Log(phaseIndex + " pahse removed");
        int startRemove = 0;
        for (int i = 0; i< phaseIndex - 1; i++)
        {
            startRemove += phaseLength[i];
        }
        for (int i = 0; i < phaseLength[phaseIndex]; i++)
        {
            phaseCompletion.RemoveAt(startRemove);
            phaseOptions.RemoveAt(startRemove);
            phaseWrongOptionResponsAudioClips.RemoveAt(startRemove);
        }
        optionResponseAudioClips.RemoveAt(phaseIndex);
        phaseLength.RemoveAt(phaseIndex);
        activePhases.RemoveAt(phaseIndex);
    }
    /// <summary>
    /// Adds the phase(s) that are triggered at the completion of the included phasenumber.
    /// </summary>
    /// <param name="finishedPhase"></param>
    void AddPhase(int finishedPhase)
    {
        // Add Phases
        List<string> addedPhases = new List<string>();
        string phase = "";
        foreach (char letter in LoadText("PHASE " + finishedPhase, "OPTIONS START")[1].Substring(9))
        {
            if (letter != ';')
            {
                phase = phase + letter;
            }
            else
            {
                activePhases.Add(phase);
                addedPhases.Add(phase);
                phase = "";
            }
        }
        foreach (string pahse in addedPhases)
        {
            Debug.Log(pahse);
        }       


        // Add keywords to 'PhaseOptions' -> make sure no spaces are included in string
        // Add file names to 'OptionResponseAudioClips' -> similar system as used above.
        foreach (string item in addedPhases)
        {
            //List<string> phaseInfo = LoadText("PHASE " + item, "PHASE END");
            int options = 0;
            foreach (string line in LoadText("OPTIONS START", "OPTIONS END", LoadText("PHASE " + item, "PHASE END")))
            {
                // All lines are searched for keyword, and filenames;
                // keywords are char 0 : ? -> check whether two spaces after each other.
                // count amount of keywords
                // add this amount of objects to 'PhaseLenght' & 'PhaseCompletion'
                int checkState = 0;
                // 0 = adding words; 1 = adding audiofiles; 
                List<string> audioFiles = new List<string>();
                string word = "";
                foreach (char letter in line)
                {
                    if (letter == '!' || letter == ';')
                    {
                        if (checkState == 0)
                        {
                            phaseOptions.Add(word);
                        }
                        else if (checkState == 1)
                        {
                            audioFiles.Add(word);
                        }
                        word = "";
                    } else
                    {
                        word = word + letter;
                    }
                    if (letter == '!') checkState++;
                }
                optionResponseAudioClips.Add(audioFiles);
                audioFiles = new List<string>();
                options++;
            }
            foreach(string line in LoadText("WRONG ANSWERS START", "WRONG ANSWERS END", LoadText("PHASE " + item, "PHASE END")))
            {
                int checkState = 0;
                string word = "";
                foreach (char letter in line)
                {
                    if (letter == ';' || letter == '!')
                    {
                        if (checkState == 0)
                        {
                            phaseWrongOptionResponsAudioClips.Add(word);
                        }else if (checkState == 1)
                        {
                            phaseSilentResponseAudioClips.Add(word);
                        }
                        word = "";
                    }else
                    {
                        word = word + letter;
                    }
                    if (letter == '!') checkState++;
                }
            }
            phaseLength.Add(options - 1);
            for (int i = 0; i < options - 1; i++)
            {
                phaseCompletion.Add(false);
            }
            options = 0;
        }
    }
    /// <summary>
    /// Returns the saught for lines inside the DataFile.
    /// </summary>
    List<string> LoadText(string startSearch, string endSearch)
    {
        List<string> foundTerms = new List<string>();
        string line;
        try
        {
            // Create a new StreamReader, tell it which file to read and what encoding the file
            // was saved as
            StreamReader theReader = new StreamReader(new MemoryStream(dataFile.bytes), Encoding.Default);
            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-intensive objects
            // instead of relying on garbage collection.
            // (Do not confuse this with the using directive for namespace at the 
            // beginning of a class!)
            using (theReader)
            {
                // While there's lines left in the text file, do this:
                bool stillLooking = true;
                bool startLooking = false;
                do
                {
                    line = theReader.ReadLine();

                    if (line != null && stillLooking)
                    {
                        // Do whatever you need to do with the text line, it's a string now
                        // In this example, I split it into arguments based on comma
                        // deliniators, then send that array to DoStuff()
                        string[] entries = line.Split(',');
                        if (line == startSearch || startLooking)
                        {
                            if (line == endSearch)
                            {
                                stillLooking = false;
                            } else
                            {
                                startLooking = true;
                                foundTerms.Add(line);
                            }
                        } 
                    }
                }
                while (line != null);
                // Done reading, close the reader and return true to broadcast success    
                theReader.Close();
                return foundTerms;
            }
        }
        // If anything broke in the try block, we throw an exception with information
        // on what didn't work
        catch{}
        return foundTerms;
    }
    /// <summary>
    /// Returns the saught for lines inside the provided list.
    /// </summary>
    List<string> LoadText(string startSearch, string endSearch, List<string> list)
    {
        List<string> foundTerms = new List<string>();
        bool startLooking = false;
        foreach (string element in list)
        {
            if (element == startSearch || startLooking)
            {
                if (element == endSearch)
                {
                    return foundTerms;
                }
                else
                {
                    startLooking = true;
                    foundTerms.Add(element);
                }
            }
        }
        return foundTerms;
    }
}