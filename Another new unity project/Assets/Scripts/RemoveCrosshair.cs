﻿using UnityEngine;
using System.Collections;

public class RemoveCrosshair : MonoBehaviour {

    public Behaviour Image;
    public GameObject Output;
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetButtonDown("Tap"))
        {
            if (Image.enabled)
            {
                Image.enabled = false;
                Output.transform.localScale = new Vector3(0, 0, 0);
            }else
            {
                Image.enabled = true;
                Output.transform.localScale = new Vector3(1, 1, 1);
            }
        }
	}
}
