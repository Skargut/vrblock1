﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class GameManagerV2 : MonoBehaviour
{
    /* Stuff that still has to be added: 
     * multiple words can be recognized for the same result.
     * Words the game responds to but do not only trigger once.
     * Phases can delete other phases. (prevent double story telling)
     */
    public Text finishedGameText;

    public Text isSpeakingOutput;
    public Text isWaitingForOutput;
    public Text checkSpeechOutput;
    
    public int startPhase;
    public AudioSource audioSource;
    public TextAsset dataFile;
    public string audioLocation;

    public AudioClip startAudioClip;
    public int DefaultSilenceDuration;
    public int currentSilenceDuration;

    public List<string> activePhases                                 = new List<string>();
    private List<List<bool>> phaseCompletion                         = new List<List<bool>>();
    private List<List<string>> phaseOptions                          = new List<List<string>>();
    private List<List<string>> optionResponseAudioClips              = new List<List<string>>();
    private List<List<string>> phaseWrongOptionResponsAudioClips     = new List<List<string>>();
    private List<string> wrongOptionResponseAudioClips               = new List<string>();
    private List<List<string>> phaseSilentResponseAudioClips         = new List<List<string>>();
    private List<string> silentResponseAudioClips                    = new List<string>();
    
    public bool isSpeaking = false;
    private int notResponding = 0;

    // Use this for initialization
    void Start()
    {
        currentSilenceDuration = DefaultSilenceDuration;
        CompletePhase(startPhase);
        wrongOptionResponseAudioClips = LoadText("WRONG ANSWER GENERAL START", "WRONG ANSWER GENERAL END");
        silentResponseAudioClips = LoadText("SILENT ANSWER GENERAL START", "SILENT ANSWER GENERAL END");
        wrongOptionResponseAudioClips.RemoveAt(0);
        silentResponseAudioClips.RemoveAt(0);
        StartCoroutine(FirstSpeech());
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //UnityEngine.SceneManagement.SceneManager.LoadScene("Level1");
        }
        
        isWaitingForOutput.text = "iswaiting for " + notResponding;

    }
    /// <summary>
    /// The speech that is played at the start of the game.
    /// </summary>
    /// <returns></returns>
    IEnumerator FirstSpeech()
    {
        audioSource.PlayOneShot(startAudioClip);
        isSpeaking = true;
        yield return new WaitForSeconds(startAudioClip.length);
        isSpeaking = false;
        StartCoroutine(NoSpeech());
    }
    /// <summary>
    /// Checks the received input on the keywords that are used inside the current phase.
    /// This function is spread over X frames to improve framerate.
    /// Only works when the game is not speaking at that moment.
    /// </summary>
    /// <param name="receivedSpeech"></param>
    /// <returns></returns>
    public void CheckSpeech(string receivedSpeech)
    {
        checkSpeechOutput.text = "Received speech: " + receivedSpeech;
        if (!isSpeaking)
        {
            isSpeaking = true;
            bool foundCorrectTerm = false;
            for (int p = 0; p < phaseOptions.Count && !foundCorrectTerm; p++)
            {
                for (int o = 0; o < phaseOptions[p].Count && !foundCorrectTerm; o++)
                {
                    int wordLength = phaseOptions[p][o].Length;
                    for (int t = 0; t + wordLength < receivedSpeech.Length + 1 && !foundCorrectTerm; t++)
                    {
                        string currentWord = receivedSpeech.Substring(t, wordLength).ToLower();
                        if ((currentWord.ToLower() == phaseOptions[p][o].ToLower() || phaseOptions[p][o] == "-"))
                        {
                            phaseCompletion[p][o] = true;
                            foundCorrectTerm = true;
                            StartCoroutine(GetSoundCorrect(p,o));
                        }
                    }
                    if (!foundCorrectTerm && phaseOptions.Count == 1 && phaseWrongOptionResponsAudioClips[0].Count != 0 && p == phaseOptions[p].Count - 1) StartCoroutine(GetSoundIncorrect(p));
                }
                if (!foundCorrectTerm && (phaseOptions.Count > 1 || phaseWrongOptionResponsAudioClips[0].Count == 0) && p == phaseOptions.Count - 1) StartCoroutine(GetSoundIncorrect());
            }
        }
    }
    /// <summary>
    /// Checks for how long you haven't spoken.
    /// Plays AudioClip when silent for too long.
    /// </summary>
    /// <returns></returns>
    IEnumerator NoSpeech()
    {
        AudioClip audioClip = new AudioClip();
        while (true)
        {
            if (isSpeaking)
            {
                notResponding = 0;
            }
            else
            {
                notResponding++;
            }
            if (notResponding == currentSilenceDuration)
            {
                currentSilenceDuration = DefaultSilenceDuration;
                if (phaseSilentResponseAudioClips.Count != 0 && phaseSilentResponseAudioClips[0].Count != 0)
                {
                    int i = 0;
                    if (activePhases[0] == "3" && activePhases[1] == "4")
                    {
                        i = 0;
                    }
                    else {
                        int l = 0;
                        foreach(string item in activePhases)
                        {
                            if (int.Parse(item) > l) l = int.Parse(item);
                        }


                        i = activePhases.IndexOf(l+"");
                        Debug.Log(l);
                        Debug.Log(i);
                    }

                    List<string> silentResponse = phaseSilentResponseAudioClips[i];
                    audioClip = Resources.Load(audioLocation + silentResponse[UnityEngine.Random.Range(0, silentResponse.Count)]) as AudioClip;
                }
                else
                {
                    audioClip = Resources.Load(audioLocation + silentResponseAudioClips[UnityEngine.Random.Range(0, silentResponseAudioClips.Count)]) as AudioClip;
                }
                audioSource.PlayOneShot(audioClip);
                isSpeaking = true;
                yield return new WaitForSeconds(audioClip.length);
                isSpeaking = false;
                notResponding = 0;
            }
            yield return new WaitForSeconds(1);
        }
    }
    /// <summary>
    /// Selects one of the X possible responses that you are able to get.
    /// </summary>
    /// <param name="optionNo"></param>
    /// <returns></returns>
    IEnumerator GetSoundCorrect(int phaseNo, int optionNo)
    {
        isSpeakingOutput.text = "iscorrect";
        yield return new WaitForSeconds(2);
        AudioClip audioClip = new AudioClip();
        audioClip = Resources.Load(audioLocation + optionResponseAudioClips[phaseNo][optionNo]) as AudioClip;
        audioSource.PlayOneShot(audioClip);
        
        yield return new WaitForSeconds(audioClip.length);
        isSpeaking = false;
        CheckPhaseCompletion();
    }
    /// <summary>
    /// Plays one of the X possible sounds when no keywords are recognized.
    /// Is used when there is only 1 phase active.
    /// </summary>
    /// <returns></returns>
    IEnumerator GetSoundIncorrect(int phaseIndexNo)
    {
        yield return new WaitForSeconds(2);
        List<string> wrongPhaseAnswers = phaseWrongOptionResponsAudioClips[phaseIndexNo];
        AudioClip audioClip = new AudioClip(); ;
        try
        {
            audioClip = Resources.Load(audioLocation + wrongPhaseAnswers[UnityEngine.Random.Range(0, wrongPhaseAnswers.Count)]) as AudioClip;
        }
        catch (Exception e)
        {
            checkSpeechOutput.text = e.Message + "\n" + e.Source;
        }
        if (audioClip != null) audioSource.PlayOneShot(audioClip);
        yield return new WaitForSeconds(audioClip.length);
        isSpeaking = false;
    }
    /// <summary>
    /// Plays one of the X possible sounds when no keywords are recognized. 
    /// Is used when there are more than 1 phase active.
    /// </summary>
    /// <returns></returns>
    IEnumerator GetSoundIncorrect()
    {
        yield return new WaitForSeconds(2);
        AudioClip audioClip = new AudioClip();
        try
        {
            audioClip = Resources.Load(audioLocation + wrongOptionResponseAudioClips[UnityEngine.Random.Range(0, wrongOptionResponseAudioClips.Count)]) as AudioClip;
        }
        catch(Exception e)
        {
            checkSpeechOutput.text = e.Message + "\n" + e.Source;
        }
        if (audioClip != null) audioSource.PlayOneShot(audioClip);
        checkSpeechOutput.text = audioClip.length + " incorrect multe";
        yield return new WaitForEndOfFrame();
        isSpeaking = false;
    }
    /// <summary>
    /// Checks all phases on whether all their keywords have been triggered.
    /// And Removes the phase that has been completed.
    /// </summary>
    void CheckPhaseCompletion()
    {
        for (int i = 0; i < phaseCompletion.Count; i++)
        {
            bool correct = true;
            foreach (bool option in phaseCompletion[i]) {
                if (!option) correct = false;
            }
            if (correct)
            {
                CompletePhase(int.Parse(activePhases[i]));
                RemovePhase(i);
            }
        }
    }
    /// <summary>
    /// Removes all variables from included phase.
    /// the included phase is the phase's POSITION INDEX inside the list.
    /// </summary>
    /// <param name="phase"></param>
    void RemovePhase(int phaseIndex)
    {
        phaseCompletion.RemoveAt(phaseIndex);
        phaseOptions.RemoveAt(phaseIndex);
        phaseWrongOptionResponsAudioClips.RemoveAt(phaseIndex);
        phaseSilentResponseAudioClips.RemoveAt(phaseIndex);
        optionResponseAudioClips.RemoveAt(phaseIndex);
        activePhases.RemoveAt(phaseIndex);
    }
    /// <summary>
    /// Adds the phase(s) that are triggered at the completion of the included phasenumber.
    /// </summary>
    /// <param name="finishedPhase"></param>
    void CompletePhase(int finishedPhase)
    {
        List<string> addedPhases = new List<string>();
        string phase = "";
        foreach (char letter in LoadText("PHASE " + finishedPhase, "OPTIONS START")[1].Substring(9)) 
        {
            if (letter != ';')
            {
                phase = phase + letter;
            }
            else
            {
                bool isActive = false;
                foreach (string currentPhases in activePhases)
                {
                    if (currentPhases == phase) isActive = true;
                }
                if (!isActive)
                {
                    if (phase != "")
                    {
                        activePhases.Add(phase);
                        addedPhases.Add(phase);
                    }
                }
                phase = "";
            }
        }
        // Add keywords to 'PhaseOptions' -> make sure no spaces are included in string
        // Add file names to 'OptionResponseAudioClips' -> similar system as used above.
        foreach (string item in addedPhases)
        {
            List<string> phaseInfo = LoadText("PHASE " + item, "PHASE END");
            List<int> options = new List<int>();
            // Adds all info for an entire phase.
            foreach (string line in LoadText("OPTIONS START", "OPTIONS END", phaseInfo))
            {
                // All lines are searched for keyword, and filenames;
                // keywords are char 0 : ? -> check whether two spaces after each other.
                // count amount of keywords
                // add this amount of objects to 'PhaseLenght' & 'PhaseCompletion'
                int checkState = 0;
                // 0 = adding words; 1 = adding audiofiles; 
                List<string> keywords = new List<string>();
                List<string> audioFiles = new List<string>();
                string word = "";
                foreach (char letter in line)
                {
                    if (letter == '!' || letter == ';')
                    {
                        if (checkState == 0)
                        {
                            keywords.Add(word);
                        }
                        else if (checkState == 1)
                        {
                            audioFiles.Add(word);
                        }
                        word = "";
                    }
                    else
                    {
                        word = word + letter;
                    }
                    if (letter == '!') checkState++;
                }
                phaseOptions.Add(keywords);
                optionResponseAudioClips.Add(audioFiles);
                options.Add(phaseOptions.Count - 1);
            }
            // Adds all string names for the wrong answers.
            foreach (string line in LoadText("WRONG ANSWERS START", "WRONG ANSWERS END", phaseInfo))
            {
                int checkState = 0;
                string word = "";
                List<string> wrongOptions = new List<string>();
                List<string> silentOptions = new List<string>();
                foreach (char letter in line)
                {
                    if (letter == ';' || letter == '!')
                    {
                        if (checkState == 0)
                        {
                            wrongOptions.Add(word);
                        }
                        else if (checkState == 1)
                        {
                            silentOptions.Add(word);
                        }
                        word = "";
                    }
                    else
                    {
                        word = word + letter;
                    }
                    if (letter == '!') checkState++;
                }
                if (wrongOptions.Count == 0 && line!="WRONG ANSWERS START") wrongOptions.Add("");
                if (silentOptions.Count == 0 && line!="WRONG ANSWERS START") silentOptions.Add("");
                phaseWrongOptionResponsAudioClips.Add(wrongOptions);
                phaseSilentResponseAudioClips.Add(silentOptions);
            }
            // If there are no wrong answers an empty array is added.
            if (LoadText("WRONG ANSWERS START", "WRONG ANSWERS END", phaseInfo).Count == 0)
            {
                phaseWrongOptionResponsAudioClips.Add(new List<string>());
                phaseSilentResponseAudioClips.Add(new List<string>());
            }
            // Adds all custom game times.
            foreach (string line in LoadText("WRONG ANSWERS END", "PHASE END", phaseInfo))
            {
                try
                {
                    if (int.Parse(line) < currentSilenceDuration) currentSilenceDuration = int.Parse(line);
                }
                catch { }
            }
            foreach (int element in options)
            {
                List<bool> completion = new List<bool>();
                for (int i = 0; i < phaseOptions[element].Count; i++)
                {
                    completion.Add(false);
                }
                phaseCompletion.Add(completion);
            }
        }
    }
    /// <summary>
    /// Returns the saught for lines inside the DataFile.
    /// </summary>
    List<string> LoadText(string startSearch, string endSearch)
    {
        List<string> foundTerms = new List<string>();
        string line;
        try
        {
            // Create a new StreamReader, tell it which file to read and what encoding the file
            // was saved as
            StreamReader theReader = new StreamReader(new MemoryStream(dataFile.bytes), Encoding.Default);
            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-intensive objects
            // instead of relying on garbage collection.
            // (Do not confuse this with the using directive for namespace at the 
            // beginning of a class!)
            using (theReader)
            {
                // While there's lines left in the text file, do this:
                bool stillLooking = true;
                bool startLooking = false;
                do
                {
                    line = theReader.ReadLine();

                    if (line != null && stillLooking)
                    {
                        // Do whatever you need to do with the text line, it's a string now
                        // In this example, I split it into arguments based on comma
                        // deliniators, then send that array to DoStuff()
                        string[] entries = line.Split(',');
                        if (line == startSearch || startLooking)
                        {
                            if (line == endSearch)
                            {
                                stillLooking = false;
                            }
                            else
                            {
                                startLooking = true;
                                foundTerms.Add(line);
                            }
                        }
                    }
                }
                while (line != null);
                // Done reading, close the reader and return true to broadcast success    
                theReader.Close();
                return foundTerms;
            }
        }
        // If anything broke in the try block, we throw an exception with information
        // on what didn't work
        catch { }
        return foundTerms;
    }
    /// <summary>
    /// Returns the saught for lines inside the provided list.
    /// </summary>
    List<string> LoadText(string startSearch, string endSearch, List<string> list)
    {
        
        List<string> foundTerms = new List<string>();
        bool startLooking = false;
        foreach (string element in list)
        {
            if (element == startSearch || startLooking)
            {
                if (element == endSearch)
                {
                    return foundTerms;
                }
                else
                {
                    startLooking = true;
                    if (element != startSearch) foundTerms.Add(element);
                }
            }
        }
        return foundTerms;
    }
}